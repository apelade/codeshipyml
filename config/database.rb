raise "No RACK_ENV found " unless ENV['RACK_ENV']
rack_env = ENV['RACK_ENV']
yml =  'config/database.yml'
config =  YAML.load(ERB.new(File.read(yml)).result)[rack_env]
ActiveRecord::Base.configurations[rack_env] = config # Not used by codeship
#
# Why does codeship have need for a config ['default_env'] below?
# See activerecord-4.1.4/lib/active_record/connection_handling.rb
# Tried ENV['RAILS_ENV'] =  ENV['RACK_ENV'] and setting on command line and exporting No
# Tried setting RAILS_ENV in config/boot.rb No
#
ActiveRecord::Base.configurations['default_env'] = config


# Setup our logger
ActiveRecord::Base.logger = logger

if ActiveRecord::VERSION::MAJOR.to_i < 4
  # Raise exception on mass assignment protection for Active Record models.
  ActiveRecord::Base.mass_assignment_sanitizer = :strict

  # Log the query plan for queries taking more than this (works
  # with SQLite, MySQL, and PostgreSQL).
  ActiveRecord::Base.auto_explain_threshold_in_seconds = 0.5
end

# Doesn't include Active Record class name as root for JSON serialized output.
ActiveRecord::Base.include_root_in_json = false

# Store the full class name (including module namespace) in STI type column.
ActiveRecord::Base.store_full_sti_class = true

# Use ISO 8601 format for JSON serialized times and dates.
ActiveSupport.use_standard_json_time_format = true

# Don't escape HTML entities in JSON, leave that for the #json_escape helper
# if you're including raw JSON in an HTML page.
ActiveSupport.escape_html_entities_in_json = false

# Now we can establish connection with our db.
ActiveRecord::Base.establish_connection(ActiveRecord::Base.configurations[Padrino.env])

# Timestamps are in the utc by default.
ActiveRecord::Base.default_timezone = :utc
